otputil
=======


Summary
-------

The otputil library provides features for online research tasks. Many of these
features are designed to extend the jsPsych framework and JATOS server platform.

Options:

- Progressive saving of trial data to a JATOS server while a task is in progress
- Encryption of jsPsych trial data, as well as more general encryption functions
- Easy saving of metadata (study, task, participant) and session information
  (e.g. whether a participant changes browser tab/window during the task)
- ... (more to be documented)

otputil is a utility developed to accelerate development of tasks for the BRAMS
online testing platform (OTP). Hence, the library's rather predictable name.
otputil may be useful in other online testing environments beyond the BRAMS OTP,
where jsPsych or JATOS are also commonly used.


Encryption features
-------------------

Encryption in otputil is based on the PGP standard, via the openpgpjs library.
By using PGP's public-key encryption, the testing environment (i.e. the
participant's browser) is provided with a "public" key that can only encrypt,
but not decrypt data. The researcher is entrusted with the "private" key to
decrypt the resulting data. If the researcher keeps control over their the
private key then any ecrypted data saved during the task cannot be decrypted by
anyone other than the researcher.

In contrast, encryption that simply uses a password would require that password
to be accessible by the testing environment (the participant's browser). In that
situation, retrieval of the password by the participant or the platform (JATOS
server administrators) could in turn compromise confidentiality of an entire
study's encrypted data. In consideration of this, we find the asymmetric
nature of public-key encryption to be much better suited to online testing than
shared-password encryption.

PGP was selected because other public-key / hybrid-encryption solutions we
reviewed were more ad-hoc in terms of the data format standard and the potential
workflow between browser, the resulting data files, and the analysis scripts
operating on the data. This included options based on openssl/libressl. S/MIME
was considered but carries considerable data overhead from its headers if
smaller packets (e.g. single-trial data) are encrypted individually. Overall,
PGP has an advantage of being a well-defined and longstanding encryption
standard, with many cross-platform applications and libraries able to generate
keys and process its encrypted data.


Documentation
-------------

TODO

For now, please refer to the source code and examples


History
-------

#### v1.x - February-March 2021

- Prerelease changes; see comments in javascript source


Authors
-------

- Nick Foster
